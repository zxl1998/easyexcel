package com.formssi.easyexcel;

import com.formssi.easyexcel.service.ExcelService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EasyexcelApplicationTests {

    @Autowired
    @Qualifier("excelServiceImpl")
    private ExcelService uploadDAO;

    @Test
    void contextLoads() {
        System.out.println(uploadDAO.findUserById(1));
    }

}
