package com.formssi.easyexcel.mapper1;

import com.formssi.easyexcel.pojo.UserExcel2;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ExcelMapper {

    void save(UserExcel2 user);

    List<UserExcel2> list();

    UserExcel2 findUserById(Integer id);

    List<UserExcel2> findAllUser();

    int update(UserExcel2 user2);
}