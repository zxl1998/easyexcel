package com.formssi.easyexcel.service;

import com.formssi.easyexcel.pojo.UserExcel2;

import java.util.List;

/**
 * @description:
 * @author: ajims
 * @time: 2021/6/29 11:29
 */
public interface ExcelService2 {

    void save(UserExcel2 user);

    List<UserExcel2> list();

    UserExcel2 findUserById(Integer id);

    List<UserExcel2> findAllUser();

    int update(UserExcel2 user2);
}
