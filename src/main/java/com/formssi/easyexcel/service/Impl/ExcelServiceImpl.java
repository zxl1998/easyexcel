package com.formssi.easyexcel.service.Impl;

import com.formssi.easyexcel.mapper1.ExcelMapper;
import com.formssi.easyexcel.pojo.UserExcel2;
import com.formssi.easyexcel.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: ajims
 * @time: 2021/6/29 11:30
 */
@Service
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    ExcelMapper excelMapper;

    @Override
    public void save(UserExcel2 user) {
        excelMapper.save(user);
    }

    @Override
    public List<UserExcel2> list() {
        return excelMapper.list();
    }

    @Override
    public UserExcel2 findUserById(Integer id) {
        return excelMapper.findUserById(id);
    }

    @Override
    public List<UserExcel2> findAllUser() {
        return excelMapper.findAllUser();
    }

    @Override
    public int update(UserExcel2 user2) {
        return excelMapper.update(user2);
    }
}
