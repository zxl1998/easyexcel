package com.formssi.easyexcel.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 多数据源配置
 */
@Configuration
public class DataSourceConfig {

//    //主数据源配置 ds1数据源
//    @Primary
//    @Bean(name = "ds1DataSourceProperties")
//    @ConfigurationProperties(prefix = "spring.datasource.ds1")
//    public DataSourceProperties ds1DataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    //主数据源 ds1数据源
//    @Primary
//    @Bean(name = "ds1DataSource")
//    public DataSource ds1DataSource(@Qualifier("ds1DataSourceProperties") DataSourceProperties dataSourceProperties) {
//        return dataSourceProperties.initializeDataSourceBuilder().build();
//    }
//配置数据源 test1DataSource
    @Bean(name="ds1DataSource")
    @ConfigurationProperties(prefix="spring.datasource.ds1")
    public DataSource testDataSource() {
    return DataSourceBuilder.create().build();
    }

//    //第二个ds2数据源配置
//    @Bean(name = "ds2DataSourceProperties")
//    @ConfigurationProperties(prefix = "spring.datasource.ds2")
//    public DataSourceProperties ds2DataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    //第二个ds2数据源
//    @Bean("ds2DataSource")
//    public DataSource ds2DataSource(@Qualifier("ds2DataSourceProperties") DataSourceProperties dataSourceProperties) {
//        return dataSourceProperties.initializeDataSourceBuilder().build();
//    }

    @Bean(name="ds2DataSource")
    @ConfigurationProperties(prefix="spring.datasource.ds2")
    public DataSource testDataSource2() {
        return DataSourceBuilder.create().build();
    }



}
