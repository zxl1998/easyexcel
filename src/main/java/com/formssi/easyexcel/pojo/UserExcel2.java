package com.formssi.easyexcel.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @description:
 * @author: ajims
 * @time: 2021/6/28 14:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserExcel2 implements Serializable {
    @ExcelProperty( "编号")
    private Integer id;
    @ExcelProperty("姓名")
    @NotNull(message = "姓名不能为空！")
    private String name;

    @Range(min=0, max=150)
    @ExcelProperty("年龄")
    private Integer age;

    @Length(min=11, max=11)
    @ExcelProperty("电话")
    private String phone;
    @ExcelProperty("性别")
    private String sex;
    @ExcelProperty("出生日期")
    @DateTimeFormat("yyyy-MM-dd")
    private Date birthday;
    @ExcelProperty("地址")
    private String address;

    @Email
    @ExcelProperty( "email")
    private String email;
    @ExcelProperty("毕业院校")
    private String college;
    @ExcelProperty("专业")
    private String major;
    @ExcelProperty("创建时间")
    private Date instData;
    @ExcelProperty("修改时间")
    private Date modiTime;
}
