/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.24 : Database - springboot_demo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`springboot_demo` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `springboot_demo`;

/*Table structure for table `user2` */

DROP TABLE IF EXISTS `user2`;

CREATE TABLE `user2` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `age` int NOT NULL,
  `phone` varchar(20) NOT NULL,
  `sex` varchar(5) NOT NULL,
  `birthday` date NOT NULL,
  `address` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `college` varchar(20) NOT NULL,
  `major` varchar(20) NOT NULL,
  `inst_data` datetime NOT NULL,
  `modi_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;

/*Data for the table `user2` */

insert  into `user2`(`id`,`name`,`age`,`phone`,`sex`,`birthday`,`address`,`email`,`college`,`major`,`inst_data`,`modi_time`) values 
(1,'小明0',0,'13713816676','男','2021-06-29','人民南路0号','1971587947@qq.com','深圳大学','计算机科学与技术','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(2,'小明1',0,'13713816676','女','2021-06-29','人民南路1号','1971587947@qq.com','深圳大学','软件工程','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(3,'小明2',0,'13713816676','男','2021-06-29','人民南路2号','1971587947@qq.com','深圳大学','计算机科学与技术','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(4,'小明3',0,'13713816676','女','2021-06-29','人民南路3号','1971587947@qq.com','深圳大学','软件工程','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(5,'小明4',0,'13713816676','男','2021-06-29','人民南路4号','1971587947@qq.com','深圳大学','计算机科学与技术','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(6,'小明5',0,'13713816676','女','2021-06-29','人民南路5号','1971587947@qq.com','深圳大学','软件工程','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(7,'小明6',0,'13713816676','男','2021-06-29','人民南路6号','1971587947@qq.com','深圳大学','计算机科学与技术','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(8,'小明7',0,'13713816676','女','2021-06-29','人民南路7号','1971587947@qq.com','深圳大学','软件工程','2021-07-01 08:55:50','2021-07-01 08:56:30'),
(9,'小明8',0,'13713816676','男','2021-06-29','人民南路8号','1971587947@qq.com','深圳大学','计算机科学与技术','2021-07-01 08:55:50','2021-07-01 08:56:31'),
(10,'小明9',0,'13713816676','女','2021-06-29','人民南路9号','1971587947@qq.com','深圳大学','软件工程','2021-07-01 08:55:50','2021-07-01 08:56:31');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
